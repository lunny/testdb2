package main

import (
    "database/sql"
	"fmt"
	
	_ "github.com/ibmdb/go_ibm_db"
	"code.gitea.io/log"
)

func main(){
    con:="HOSTNAME=localhost;DATABASE=testdb;PORT=50000;UID=db2inst1;PWD=password"
 db, err:=sql.Open("go_ibm_db", con)
    if err != nil{
		log.Error("%v", err)
	}
	if err := db.Ping(); err != nil {
		log.Error("%v", err)
	}

	/*_,err=db.Exec("DROP table MY_TABLE")
	if err != nil {
		log.Error("%v", err)
	}*/

	_, err = db.Exec("create table MY_TABLE (NAME varchar(20))")
	if err != nil {
		log.Error("%v", err)
	}

	_, err = db.Exec("insert into MY_TABLE (NAME) values(?)", "abc")
	if err != nil {
		log.Error("%v", err)
	}

	var mytext string
	err = db.QueryRow("select * from MY_TABLE").Scan(&mytext)
	if err != nil {
		log.Error("%v", err)
	}

	fmt.Println(mytext)
	db.Close()
}